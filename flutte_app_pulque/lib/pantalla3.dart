// ignore_for_file: use_key_in_widget_constructors
//import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'package:flutter/material.dart';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

Future<String> loadAsset() async {
  return await rootBundle.loadString('assets/config.json');
}

class MainApp3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Establecimientos',
      //home: Container(
      // decoration: const BoxDecoration(
      // image: DecorationImage(
      // image: AssetImage("assets/img2.jpeg"),
      // fit: BoxFit.cover,
      //),
      //),
      //child:tex,
      //),
      // );
      //}
      //}
      theme: ThemeData(primarySwatch: Colors.lightBlue),
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 181, 209, 224),
        body: Center(
          child: Stack(
            alignment: Alignment.center,
            children: [
              Image.asset('assets/img2.jpeg'),
              Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                alignment: Alignment.center,
                child: ListView(
                  children: [
                    Container(
                      child: RaisedButton(
                        child: Text('Inicio'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        color: Color.fromARGB(255, 233, 135, 14),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Nombre del establecimiento: PulqueLokos'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title:
                            Text('Ubicacion: AV.Hidalgo #2 Col.Centro Cuautla'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Tipos de pulque: Sabores fresa y natural'),
                        subtitle: Text('Natural/Sabores'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Costos: Desde 15-50'),
                        subtitle: Text('Economico/Costoso'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Tamaño: 1 Litro, 1/2 Litro, vaso'),
                        subtitle: Text('Ch/M/G'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Correo: pulquelokos@hotmail.com'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Telefono: 7352666778'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Redes: FB,Whats,IG'),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
