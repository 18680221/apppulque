import 'package:flutte_app_pulque/main_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

Future<String> loadAsset() async {
  return await rootBundle.loadString('assets/config.json');
}

/* return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),*/
class MainApp2 extends StatelessWidget {
  // nombre nuestra classe
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PulqueAPP",
      theme: ThemeData(primarySwatch: Colors.lightBlue),
      home: Scaffold(
        //appBar: AppBar(title: Text("Second Route")),

        backgroundColor: Color.fromARGB(255, 181, 209, 224),

        body: Center(
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/img1.jpeg"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              //Image.asset('assets/img1.jpeg'),

              Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                alignment: Alignment.center,
                child: ListView(
                  children: [
                    //Inicia
                    Container(
                      child: ListTile(
                        title: Text('Nombre del establecimiento:'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Ubicacion:'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Tipos de pulque:'),
                        subtitle: Text('Natural/Sabores'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Costos:'),
                        subtitle: Text('Economico/Costoso'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Tamaño:'),
                        subtitle: Text('Ch/M/G'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Correo:'),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        title: Text('Telefono:'),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: (FloatingActionButton(
                  onPressed: () {
                    //boton para regresar al menu
                    //Navigator.pushNamed(context, '/main_app.dart');
                    /*Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MainApp()),
                    );*/
                    Navigator.pop(context);
                  },
                  child: Icon(Icons.home),
                )),
              ),
              /* Container(
                child: RaisedButton(
                  child: Text('Nuevo'),
                  onPressed: () {},
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                ),
              ),*/
            ],
          ),
        ),
      ),
    );
  }
}
