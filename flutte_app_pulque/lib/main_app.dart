// ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors

import 'package:flutte_app_pulque/pantalla2.dart';
import 'package:flutte_app_pulque/pantalla3.dart';
import 'package:flutter/cupertino.dart';

import 'dart:async';

import 'package:flutter/material.dart';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

Future<String> loadAsset() async {
  return await rootBundle.loadString('assets/config.json');
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/inicio1.png"),
            fit: BoxFit.cover,
          ),
        ), //child:tex,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Container(
            child: Center(
              child: ListTile(
                  title: Text(
                      'El contenido alcohólico varió en función del tiempo de fermentación alcanzando porcentajes de 10.35 (v/v) y 9.01 (v/v) determinados por cromatografía de gases y espectroscopia Raman para la etapa denominada «corrida». El producto terminado tiene un elevado contenido de proteínas (1 g/L), azúcares reductores (4.75 g/L) y poblaciones microbianas en ordenes de 45x108 UFC/mL para Saccharomyces sp, 41x107 UFC/mL para Zymomonas sp y 34x106 UFC/mL para Lactobacillus sp, que le dan a la bebida un elevado valor nutricional y los microorganismos aislados podrían tener un efecto benéfico sobre sistema digestivo al ser consumido por vía oral.')),
            ),
          ),

          /*Row(children: [
            //Container(width: 200, child: IconButton("assets/icno1.png")),
            //Container(width: 200, child: IconButton("assets/icno1.png")),
            Container(width: 200, child: IconButton(Icons.add_business_sharp))
          ])*/
          SizedBox(
            height: 50,
          ),
          Row(
            children: [
              FloatingActionButton(
                onPressed: () {
                  // Navigator.pushNamed(context, '/pantallas2');
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MainApp2()),
                  );
                },
                child: Icon(Icons.add_business),
              ),
              FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MainApp3()),
                  );
                },
                child: Icon(Icons.visibility_outlined),
              )
            ],
          ),
        ]),
      ),
    );
  }
}
